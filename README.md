## Boca Juniors Team(incomplete)
This project consists of several proofs of concept:
* H2 database
* Python script to get players and stats from a website and insert them in a CSV.
* Conversions from XML to JSON, JSON to a Java Object using Jackson.
* Validator for player's request, validates if the country is a valid one.
* Simple Controller/Service/DAO for getting and saving players and stats from a soccer team to the database.
* Messaging service implemented with ActiveMQ to select, update, insert and delete players and stats.
* Scheduler updates notification topic with new stats that are sent via publisher, updates scores of each player(incomplete).


//TODO falta descripcion


### Python script
[Python Script for collecting team's information](https://bitbucket.org/calixto_gonzalez/scrapping-boca-juniors-team)

This script was created for collecting any team's information through scrapping.

### H2 Database
Implemented this database since it can be embedded inside spring boot and for testing purposes it does the job.

