INSERT INTO PLAYERS(PLAYER_ID,NAME,LASTNAME,COUNTRY,AGE) VALUES
(1,'Darío','Benedetto','Argentina',28),
(2,'Cristian','Pavón','Argentina',22),
(3,'Paolo','Goltz','Argentina',33),
(4,'Frank Yusty','Fabra','Colombia',27),
(5,'Pablo','Pérez','Argentina',32),
(6,'Edwin Andres','Cardona','Colombia',25);



INSERT INTO STATS(PLAYER_ID,RIVAL,RESULT,GOALS,YELLOWS,REDS,COMMENTARY,TIME) VALUES
(1,'River', '2-1' ,2,0,0,'Jugo bien',NOW()),
(2,'River', '2-1' ,0,1,0,'Lesionado',NOW()),
(3,'River', '2-1' ,0,1,0,'Buen partido',NOW() ),
(3,'River', '2-1' ,5,1,0,'Buen partido',NOW() ),
(4,'River', '2-1' ,0,0,0,'Flojo',NOW() ),
(5,'River', '2-1' ,0,1,0,'Deja que desear',NOW() ),
(6,'River', '2-1' ,1,0,0,'La rompio',NOW() ),
(6,'River', '2-1' ,4,0,0,'La rompio',NOW() );


