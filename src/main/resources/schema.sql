DROP ALL OBJECTS;
CREATE TABLE IF NOT EXISTS PLAYERS(
PLAYER_ID INT PRIMARY KEY,
NAME VARCHAR,
LASTNAME VARCHAR,
COUNTRY VARCHAR,
AGE INT);


CREATE TABLE IF NOT EXISTS STATS(
ID_STATS INT AUTO_INCREMENT PRIMARY KEY,
PLAYER_ID INT,
FOREIGN KEY (PLAYER_ID) REFERENCES PLAYERS(PLAYER_ID),
RIVAL VARCHAR,
RESULT VARCHAR,
GOALS INT,
YELLOWS INT,
REDS INT,
COMMENTARY VARCHAR,
TIME DATE);

