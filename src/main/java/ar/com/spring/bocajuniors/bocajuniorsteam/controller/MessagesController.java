package ar.com.spring.bocajuniors.bocajuniorsteam.controller;


import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.MessagingException;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.MessageService;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.PlayerService;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.StatService;
import ar.com.spring.bocajuniors.bocajuniorsteam.wrapper.ErrorWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessagesController {

    private MessageService messageService;
    private PlayerService playerService;
    private StatService statService;


    /**
     * @param messageService {@link MessageService}
     */
    @Autowired
    public MessagesController(final MessageService messageService, final PlayerService playerService, final StatService statService) {
        this.messageService = messageService;
        this.playerService = playerService;
        this.statService=statService;

    }


    /**
     * @param message
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity messageSender(@RequestBody final String message) {
        try {
            return ResponseEntity.ok(messageService.messageSender(message));
        } catch (MessagingException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param player {@link Player}
     * @return
     */
    @RequestMapping(value = "/player",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity messageInsertPlayer(@RequestBody final Player player) {
        try {
             playerService.insertPlayer(player);
            return ResponseEntity.ok(messageService.messageInsertPlayer(player));
        } catch (MessagingException | DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     *
     * @param stat {@link Stat}
     * @return
     */
    @RequestMapping(value = "/stat",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity messageInsertStat(@RequestBody final Stat stat) {
        try {
            statService.insertStat(stat);
            return ResponseEntity.ok(messageService.messageInsertStat(stat));
        } catch (MessagingException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




}
