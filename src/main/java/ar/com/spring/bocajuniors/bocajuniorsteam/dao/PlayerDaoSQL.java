package ar.com.spring.bocajuniors.bocajuniorsteam.dao;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;

import java.util.List;

/**
 * Interface de PlayerDao
 */
public interface PlayerDaoSQL {
    /**
     * @return List {@link List} List con Players.
     * @throws DBException {@link DBException}
     */
    List<Player> getAllPlayers() throws DBException;

    /**
     * @param playerRequest {@link ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest} esta clase contiene el id del player a buscar.
     * @return Player {@link Player} retorna un Player segun Id.
     * @throws DBException {@link DBException}
     */
    Player getPlayerById(int playerRequest) throws DBException;

    /**
     * @param country {@link String}
     * @return List {@link List} List of players by country.
     * @throws DBException {@link DBException}
     */
    List<Player> getPlayersByCountry(String country) throws DBException;

    void deletePlayerById(int playerId) throws DBException;

    void insertPlayer(Player player) throws DBException;
}
