package ar.com.spring.bocajuniors.bocajuniorsteam.controller;


import ar.com.spring.bocajuniors.bocajuniorsteam.service.ActiveMQService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * ActiveMQ controller
 */
@RestController
@RequestMapping("/msg")
public class ActiveMQController {

    @Autowired
    private ActiveMQService activeMQService;

    /**
     *
     * @param s {@link String}
     */
    @RequestMapping(value = "/stat", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    public void sender(@RequestBody final String s) {
        try {
            activeMQService.sender(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
