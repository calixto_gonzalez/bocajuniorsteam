package ar.com.spring.bocajuniors.bocajuniorsteam.service;

import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQL;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader.PlayerLoader;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static sun.audio.AudioPlayer.player;

/**
 * Esto es el servicio de Players el cual deberia contener la logica de negocios.
 */
@Service
public class StatService {
    private PlayerDaoSQL playerDaoSQLM;
    private PlayerLoader playerLoader;
    private StatDaoSQLImple statDao;

    /**
     * Inyecta los beans en el contructor.
     *
     * @param playerDaoSQL {@link PlayerDaoSQL}
     * @param playerLoader {@link PlayerLoader}
     */
    @Autowired
    public StatService(final PlayerDaoSQL playerDaoSQL, final PlayerLoader playerLoader) {
        this.playerDaoSQLM = playerDaoSQL;
        this.playerLoader = playerLoader;
    }

    /**
     * @return List {@link List} List of Players with Stats.
     * @throws DBException {@link DBException}
     */
    public List<Player> getAllStats() throws DBException {
        return playerLoader.loadStats(playerDaoSQLM.getAllPlayers());

    }

    public void insertStat(final Stat stat) {
        statDao.insertStat(stat);
    }
}
