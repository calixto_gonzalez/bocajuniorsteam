package ar.com.spring.bocajuniors.bocajuniorsteam.exception;

/**
 * Enum con los errores (una cuestion de prolijidad.
 */
public enum ErrorEnum {
    DATABASEERROR("General data base error."),
    BLANK("Field should no be blank."),
    NULL("Field should no be null."),
    NOTLETTERS("Field has invalid symbols."),
    NOTACOUNTRY("Field should be a valid country."),
    USERNOTFOUNDERROR("Registry not found."),
    MESSAGINGERROR("Message could not be send."),
    DUPLICATEKEY("Player already exists.");



    private String msg;

    /**
     *
     * @param msg {@link String}
     */
    ErrorEnum(final String msg) {
        this.msg = msg;
    }

    /**
     * Gets msg.
     *
     * @return Value of msg.
     */
    public String getMsg() {
        return msg;
    }


}
