package ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader;

import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Loader de players.
 */
@Component
public class PlayerLoader {

    private StatDaoSQLImple statDaoSQLImple;

    /**
     *
     * @param statDaoSQLImple {@link StatDaoSQLImple} inyecto statDao en constructor.
     */
    @Autowired
    public PlayerLoader(final StatDaoSQLImple statDaoSQLImple) {
        this.statDaoSQLImple = statDaoSQLImple;
    }

    /**
     *
     * @param players {@link Player}
     * @return List {@link List} List of Players (cargados)
     * @throws DBException {@link DBException}
     */
    public List<Player> loadStats(final List<Player> players) throws DBException {

        if (players == null || players.isEmpty()) {
            return new ArrayList<>();

        } else {

            for (Player player : players) {
                player.setStatList(statDaoSQLImple.getAllStatsByPlayer(player.getPlayerId()));
            }
            return players;

        }


    }


}
