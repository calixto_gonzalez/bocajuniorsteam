package ar.com.spring.bocajuniors.bocajuniorsteam.service;

import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Listener;
import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Publisher;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.MessagingException;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    private Publisher publisher;
    private Listener listener;

    public MessageService(Publisher publisher, Listener listener) {
        this.listener = listener;
        this.publisher = publisher;
    }

    /**
     * @param message
     * @return
     * @throws MessagingException
     */
    public String messageSender(final String message) throws MessagingException {
        return publisher.sendMessageToQueue(message);
    }


    public String messageInsertPlayer(final Player player) {
        publisher.sendMessageToQueue(player);
        return "no me jodas";
    }

    public String messageInsertStat(final Stat stat) {
        publisher.sendMessageToQueue(stat);
        return "todo bien";
    }
}
