package ar.com.spring.bocajuniors.bocajuniorsteam.task;

import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Publisher;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQL;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.PlayerService;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.StatService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Scheduler {

    private PlayerService playerService;
    private StatService statService;
    private StatDaoSQLImple statDaoSQLImple;
    private Publisher publisher;

    public Scheduler(final PlayerService playerService, final StatService statService, final StatDaoSQLImple statDaoSQLImple, Publisher publisher) {
        this.playerService = playerService;
        this.statService = statService;
        this.statDaoSQLImple = statDaoSQLImple;
        this.publisher = publisher;
    }


    @Scheduled(fixedDelay = 30000, initialDelay = 15000)
    public void mostGoals() throws DBException {
        final List<Stat> mostGoals = statDaoSQLImple.getMostGoals();
        if (mostGoals.size() == 1) {
            Player player=playerService.getPlayerById(mostGoals.get(0).getPlayerId());
            publisher.sendMessageToTopicNotification("El goleador actual es "+player.getName()+" con "+mostGoals.get(0).getGoals()+" goles.");
        } else {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Los goleadores son ");
            for (int i=0;i<mostGoals.size();i++) {
                Player player = playerService.getPlayerById(mostGoals.get(i).getPlayerId());
                if (i+1==mostGoals.size()) {
                    stringBuilder.append(player.getName());
                    stringBuilder.append(" con "+mostGoals.get(i).getGoals()+" goles.");
                }else {
                    stringBuilder.append(player.getName() + ", ");
                }

            }
            publisher.sendMessageToTopicNotification(stringBuilder.toString());

        }

    }


}
