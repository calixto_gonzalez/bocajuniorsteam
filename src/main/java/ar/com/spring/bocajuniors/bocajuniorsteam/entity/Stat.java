package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Entidad Stat
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "stat")
public class Stat {

    /* ATRIBUTOS
         aributos de la entidad.
         attributes of the entity.
     */
    @XmlElement(name = "id_stats")
    private int idStats;
    @XmlElement(name = "player_id")
    private int playerId;
    @XmlElement(name = "rival")
    private String rival;
    @XmlElement(name = "result")
    private String result;
    @XmlElement(name = "goals")
    private int goals;
    @XmlElement(name = "yellows")
    private int yellows;
    @XmlElement(name = "reds")
    private int reds;
    @XmlElement(name = "commentary")
    private String commentary;


    private Date time;

    /**
     * Constructor vacio.
     */
    public Stat() {

    }

    /**
     * @param playerId   int
     * @param idStats    int
     * @param result     int
     * @param goals      int
     * @param yellows    int
     * @param reds       int
     * @param commentary {@link String}
     * @param time       {@link Date}
     * @param rival      {@link String}
     */
    public Stat(final int playerId, final int idStats, final String result, final int goals, final int yellows,
                final int reds, final String commentary,
                final Date time, final String rival) {

        this.playerId = playerId;
        this.idStats = idStats;
        this.result = result;
        this.goals = goals;
        this.yellows = yellows;
        this.reds = reds;
        this.commentary = commentary;
        this.time = time;
        this.rival = rival;
    }


    /**
     * Gets result.
     *
     * @return Value of result.
     */
    public String getResult() {
        return result;
    }

    /**
     * Gets playerId.
     *
     * @return Value of playerId.
     */

    public int getPlayerId() {
        return playerId;
    }

    /**
     * Gets goals.
     *
     * @return Value of goals.
     */
    public int getGoals() {
        return goals;
    }

    /**
     * Gets id.
     *
     * @return Value of id.
     */
    public int getIdStats() {
        return idStats;
    }

    /**
     * Sets new time.
     *
     * @param time New value of time.
     */
    public void setTime(final Date time) {
        this.time = time;
    }

    /**
     * Gets rival.
     *
     * @return Value of rival.
     */

    public String getRival() {
        return rival;
    }

    /**
     * Sets new result.
     *
     * @param result New value of result.
     */
    public void setResult(final String result) {
        this.result = result;
    }

    /**
     * Gets reds.
     *
     * @return Value of reds.
     */
    public int getReds() {
        return reds;
    }

    /**
     * Sets new rival.
     *
     * @param rival New value of rival.
     */

    public void setRival(final String rival) {
        this.rival = rival;
    }

    /**
     * Gets commentary.
     *
     * @return Value of commentary.
     */
    public String getCommentary() {
        return commentary;
    }

    /**
     * Sets new commentary.
     *
     * @param commentary New value of commentary.
     */
    public void setCommentary(final String commentary) {
        this.commentary = commentary;
    }

    /**
     * Sets new goals.
     *
     * @param goals New value of goals.
     */
    public void setGoals(final int goals) {
        this.goals = goals;
    }

    /**
     * Sets new reds.
     *
     * @param reds New value of reds.
     */
    public void setReds(final int reds) {
        this.reds = reds;
    }

    /**
     * Gets time.
     *
     * @return Value of time.
     */
    public Date getTime() {
        return time;
    }

    /**
     * Sets new yellows.
     *
     * @param yellows New value of yellows.
     */
    public void setYellows(final int yellows) {
        this.yellows = yellows;
    }

    /**
     * Gets yellows.
     *
     * @return Value of yellows.
     */
    public int getYellows() {
        return yellows;
    }

    /**
     * Sets new id.
     *
     * @param idStats New value of id.
     */

    public void setIdStats(final int idStats) {
        this.idStats = idStats;
    }

    /**
     * Sets new playerId.
     *
     * @param playerId New value of playerId.
     */

    public void setPlayerId(final int playerId) {
        this.playerId = playerId;
    }

}
