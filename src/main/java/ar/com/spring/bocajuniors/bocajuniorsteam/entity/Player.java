package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

import java.util.List;

/**
 * Entidad player
 */
public class Player {
    private int playerId;
    private String name;
    private String lastname;
    private String country;
    private int age;
    private List<Stat> statList;

    /**
     * Constructor vacio, es necesario para Spring (y para java si fuese a ser heredada la clase).
     */
    public Player() {
    }

    /**
     *
     * @param playerId int
     * @param name {@link String}
     * @param lastname {@link String}
     * @param country {@link String}
     * @param age int
     * @param statList {@link List}
     */
    public Player(final int playerId, final String name, final String lastname, final String country, final int age, final List<Stat> statList) {
        this.playerId = playerId;
        this.name = name;
        this.lastname = lastname;
        this.country = country;
        this.age = age;
        this.statList = statList;
    }

    /**
     *
     * @param playerId int
     * @param name {@link String}
     * @param lastname {@link String}
     * @param country {@link String}
     * @param age int
     *
     */
    public Player(final int playerId, final String name, final String lastname, final String country, final int age) {
        this.name = name;
        this.lastname = lastname;
        this.country = country;
        this.age = age;
        this.playerId = playerId;
    }


    /**
     * @return getter
     */
    public String getName() {
        return name;
    }

    /**
     * @param name {@link String}
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return lastname {@link String}
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname {@link String}
     */
    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return country {@link String}
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country {@link String}
     */
    public void setCountry(final String country) {
        this.country = country;
    }


    /**
     * Gets playerId.
     *
     * @return Value of playerId.
     */
    public int getPlayerId() {
        return playerId;
    }


    /**
     * Sets new playerId.
     *
     * @param playerId New value of playerId.
     */
    public void setPlayerId(final int playerId) {
        this.playerId = playerId;
    }

    /**
     * Gets age.
     *
     * @return Value of age.
     */
    public int getAge() {
        return age;
    }


    /**
     * Sets new age.
     *
     * @param age New value of age.
     */
    public void setAge(final int age) {
        this.age = age;
    }

    /**
     * Gets statList.
     *
     * @return Value of statList.
     */
    public List<Stat> getStatList() {

        return statList;
    }

    /**
     * Sets new statList.
     *
     * @param statList New value of statList.
     */
    public void setStatList(final List<Stat> statList) {
        this.statList = statList;
    }
}
