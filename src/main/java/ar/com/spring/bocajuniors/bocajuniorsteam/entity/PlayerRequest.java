package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

/**
 * Entidad PlayerRequest creada por la necesidad de mappear un json a un objecto.
 */
public class PlayerRequest {
    private int playerRequest;


    /**
     * Constructor vacio
     */
    public PlayerRequest() {
    }

    /**
     *
     * @param playerRequest playerRequest
     */
    public PlayerRequest(final int playerRequest) {
        this.playerRequest = playerRequest;
    }

    /**
     *
     * @return playerRequest
     */
    public int getIdPlayer() {
        return playerRequest;
    }

    /**
     * MARSHALLING MIRA EL NOMBRE DEL SETTER
     * @param playerRequest int
     * @return playerRequest {@link PlayerRequest}
     */
    public PlayerRequest setIdPlayer(final int playerRequest) {
        this.playerRequest = playerRequest;
        return this;
    }
}
