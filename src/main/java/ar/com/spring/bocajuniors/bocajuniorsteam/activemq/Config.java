package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * Configuration class used by Spring Boot to initialize JmsListenerContainerFactory and the Queue and Topic.
 */
@Configuration
@EnableJms
public class Config {

    private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);

    private String queueName;
    private String topicName;

    /**
     * .
     *
     * @param topicName {@link String}
     * @param queueName {@link String}
     */
    public Config(@Value("${activemq.config.topic.name:aca.va.el.nom.por.def}") final String topicName,
                  @Value("${activemq.config.queue.name}") final String queueName) {

        this.queueName = queueName;
        this.topicName = topicName;

    }

    /**
     * Creates a JmsListenerContainerFactory bean
     * Queue
     *
     * @param connectionFactory the connection factory
     * @param configurer        the default listener
     * @return the jms listener container factory bean
     */
    @Bean
    public JmsListenerContainerFactory<?> queueListenerFactory(final ConnectionFactory connectionFactory,
                                                               final DefaultJmsListenerContainerFactoryConfigurer configurer) {
        LOGGER.debug("JmsConfiguration - init");
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);


        // You could still override some of Boot's default if necessary.
        LOGGER.debug("JmsConfiguration - end - created factory: {}", factory);
        return factory;
    }

    /**
     * Creates a JmsListenerContainerFactory bean
     * Topic
     *
     * @param connectionFactory the connection factory
     * @param configurer        the default listener
     * @return the jms listener container factory bean
     */
    @Bean
    public JmsListenerContainerFactory<?> topicListenerFactory(final ConnectionFactory connectionFactory,
                                                               final DefaultJmsListenerContainerFactoryConfigurer configurer) {


        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(true);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    /**
     * Queue Bean
     * @return ActiveMQQueue {@link Queue}
     */
    @Bean
    public Queue queue() {
        return new ActiveMQQueue(queueName);
    }

    /**
     * Topic Bean
     * @return ActiveMQTopic {@link Topic}
     */
    @Bean
    public Topic topic() {
        return new ActiveMQTopic(topicName);
    }


}
