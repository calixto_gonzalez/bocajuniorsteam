package ar.com.spring.bocajuniors.bocajuniorsteam.dao;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sun.audio.AudioPlayer;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object para buscar informacion sobre las estadisticas de los jugadores en la base de datos.
 */
@Repository
public class StatDaoSQLImple {
    private static final String QUERY_DELETE_STAT_BY_ID = "delete from stats where PLAYER_ID= ?";
    private static final String QUERY_GET_MOST_GOALS="\n" +
            "select player_id, sum(goals) as goals\n" +
            "from stats\n" +
            "group by player_id\n" +
            "having SUM(goals) =\n" +
            "(\n" +
            "select max(c) from (\n" +
            "select sum(goals) as c\n" +
            "from stats\n" +
            "group by player_id\n" +
            ")\n" +
            ")";
    private static final String QUERY_INSERT_STAT ="INSERT INTO STATS(PLAYER_ID,RIVAL,RESULT,GOALS,YELLOWS,REDS,COMMENTARY,TIME) VALUES\n" +
            "(?,?,?,?,?,?,?,?)" ;

    private JdbcTemplate jdbcTemplate;

    /**
     * @param jdbcTemplate {@link JdbcTemplate} conector inyectado.
     */
    @Autowired
    public StatDaoSQLImple(final JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    /**
     * @param playerRequest {@link ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest}
     * @return List {@link List} List con Stats y Players.
     * @throws DBException {@link DBException}
     */
    public List<Stat> getAllStatsByPlayer(final int playerRequest) throws DBException {
        try {
            return (List<Stat>) jdbcTemplate.query("select * from stats where PLAYER_ID = (" + playerRequest + ");",
                    new BeanPropertyRowMapper(Stat.class));
        } catch (EmptyResultDataAccessException e) {
            throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        } catch (DataAccessException e) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg());
        }
    }
    @Transactional
    public void deleteStatById(final int id) throws DBException {

        try {
            jdbcTemplate.update(QUERY_DELETE_STAT_BY_ID, new Object[]{id});
        } catch (EmptyResultDataAccessException erdae) {
            throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        } catch (DataAccessException e) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg(), e);
        }
    }


    public List<Stat> getMostGoals() {
        return (List<Stat>) jdbcTemplate.query(QUERY_GET_MOST_GOALS,new BeanPropertyRowMapper(Stat.class));

    }
//    @Override
//    public void insertPlayer(final Player player) throws DBException {
//        try {
//            jdbcTemplate.update(QUERY_INSERT_PLAYER,
//                    new Object[]{player.getPlayerId(),player.getName(),player.getLastname(),player.getCountry(),player.getAge()});
//        } catch (DuplicateKeyException e) {
//            throw new DBException(ErrorEnum.DUPLICATEKEY.getMsg(),e);
//        }catch (DataAccessException e) {
//            throw new DBException(ErrorEnum.DATABASEERROR.getMsg(),e);
//        }
//    }
    public void insertStat(final Stat stat) {
//        (PLAYER_ID,RIVAL,RESULT,GOALS,YELLOWS,REDS,COMMENTARY,TIME)
        jdbcTemplate.update(QUERY_INSERT_STAT,
                new Object[]{stat.getPlayerId(),stat.getRival(),stat.getResult(),stat.getGoals(),stat.getYellows(),stat.getReds(),stat.getCommentary(),new Date()});
    }
}
