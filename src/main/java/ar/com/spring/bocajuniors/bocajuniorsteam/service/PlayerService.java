package ar.com.spring.bocajuniors.bocajuniorsteam.service;


import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Publisher;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQL;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader.PlayerLoader;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Esto es el servicio de Players el cual deberia contener la logica de negocios.
 */
@Service
public class PlayerService {

    private Publisher publisher;
    private PlayerDaoSQL playerDao;
    private PlayerLoader playerLoader;

    /**
     * Inyecta los beans en el contructor.
     *
     * @param playerDao    {@link PlayerDaoSQL}
     * @param playerLoader {@link PlayerLoader}
     */
    @Autowired
    public PlayerService(final PlayerDaoSQLImple playerDao, final PlayerLoader playerLoader, final Publisher publisher) {
        this.playerDao = playerDao;
        this.playerLoader = playerLoader;
        this.publisher = publisher;
    }

    /**
     * @return List {@link List} List of Players.
     * @throws DBException {@link DBException}
     */
    public List<Player> getAllPlayers() throws DBException {
        return playerDao.getAllPlayers();
    }

    /**
     * @param idPlayer int
     * @return player {@link Player}
     * @throws DBException {@link DBException}
     */
    public Player getPlayerById(final int idPlayer) throws DBException {
        return playerDao.getPlayerById(idPlayer);
    }

    /**
     * @param country {@link String}
     * @return List {@link List}
     * @throws DBException {@link DBException} Exception lanzada a la capa del controlador.
     */
    public List<Player> getPlayersByCountry(final String country) throws DBException {

        return playerDao.getPlayersByCountry(country);
    }

    /**
     * @param country int
     * @return List {@link List}
     * @throws DBException {@link DBException}
     */
    public List<Player> getPlayersByCountryData(final String country) throws DBException {

        List<Player> playerList = playerDao.getPlayersByCountry(country);
        return playerLoader.loadStats(playerList);

    }

    public void deletePlayerById(final int playerId) throws DBException {
        playerDao.deletePlayerById(playerId);
        publisher.sendMessageToTopicNotification(("Player: " + playerId + " was deleted"));

        //Todo falta agregar try and catch ambos generan mensajes a sus respectivas queues.

    }

    public void insertPlayer(final Player player) throws DBException {
        playerDao.insertPlayer(player);
    }


}
