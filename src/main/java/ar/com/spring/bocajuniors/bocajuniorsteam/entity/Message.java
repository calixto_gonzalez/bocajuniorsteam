package ar.com.spring.bocajuniors.bocajuniorsteam.entity;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Message {
    private int receiversId;
    private int emisorId;
    private String message;

}
