package ar.com.spring.bocajuniors.bocajuniorsteam.exception;

/**
 * StringException used for encapsulation of errors.
 */
public class StringException extends IllegalArgumentException {


        /**
         *
         * @param s {@link String}
         */
        public StringException(final String s) {
            super(s);
        }

        /**
         *
         * @param s {@link String}
         * @param throwable {@link Throwable}
         */
        public StringException(final String s, final Throwable throwable) {
            super(s, throwable);
        }

}
