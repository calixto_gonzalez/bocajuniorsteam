package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Message;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.MessagingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;

/**
 * Publisher sends te messages to either a {@link Queue} or a {@link Topic}.
 */
@Component
public class Publisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(Publisher.class);


    private JmsTemplate jmsTemplate;
    private Queue queue;
    private Topic topic;
    private ObjectMapper mapper;
    @Value("${activemq.config.topic.notification.name}")
    private String notificationTopicName;

    /**
     * @param jmsTemplate {@link JmsTemplate}
     * @param queue       {@link Queue}
     * @param topic       {@link Topic}
     * @param mapper      {@link ObjectMapper}
     */
    @Autowired
    public Publisher(final JmsTemplate jmsTemplate, final Queue queue, final Topic topic, final ObjectMapper mapper) {

        this.jmsTemplate = jmsTemplate;
        this.queue = queue;
        this.topic = topic;
        this.mapper = mapper;
        LOGGER.debug("Levanto conexion");
        LOGGER.debug("Queue's name:" + queue.toString());
        LOGGER.debug("Topic's name:" + topic.toString());


    }
//    //si lo haces asi deberias mockear la inyeccion, "inyectmock" algo por el estilo.
//    @Autowired
//    ObjectMapper mapper;


    /**
     * Implements the interface method
     *
     * @param stat {@link Stat} the message
     */
    public void sender(final Stat stat) {


        String message = null;

        try {
            message = mapper.writeValueAsString(stat);
            LOGGER.debug("Message to send: {}", message);
            jmsTemplate.convertAndSend(this.queue, message);
        } catch (JsonProcessingException e) {

            LOGGER.error(e.getMessage(), e);
        }


    }

    /**
     * @param stat {@link Stat}
     */
    public void senderToTopic(final Stat stat) {


        String message = null;

        try {
            message = mapper.writeValueAsString(stat);
            LOGGER.debug("Message to send: {}", message);
            jmsTemplate.convertAndSend(this.topic, message);
        } catch (JsonProcessingException e) {

            LOGGER.error(e.getMessage(), e);
        }
    }

    //    @Bean
//    public Queue queue() {
//        return new ActiveMQQueue(queueName);
//    }
    public String sendMessageToTopic(final Message message) {
        try {
            jmsTemplate.convertAndSend(new ActiveMQTopic(Integer.toString(message.getEmisorId())), message.getMessage());
            return "message sent.";
        } catch (JmsException e) {
            throw new MessagingException(ErrorEnum.MESSAGINGERROR.getMsg());
        }

    }

    public String sendMessageToQueue(final Player player) throws MessagingException {
        try {
            String message = mapper.writeValueAsString(player);
            jmsTemplate.convertAndSend(new ActiveMQQueue("players"), message);
            return "message sent.";
        } catch (JmsException e) {
            throw new MessagingException(ErrorEnum.MESSAGINGERROR.getMsg());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;

    }

    public String sendMessageToQueue(final String message) throws MessagingException {
        try {
            jmsTemplate.convertAndSend(new ActiveMQQueue("messages"), message);
            return "message sent.";
        } catch (JmsException e) {
            throw new MessagingException(ErrorEnum.MESSAGINGERROR.getMsg());

        }
    }
    public String sendMessageToQueue(final Stat stat) throws MessagingException {

        try {
            String message = mapper.writeValueAsString(stat);
            jmsTemplate.convertAndSend(new ActiveMQQueue("stats"), message);
            return "message sent.";
        } catch (JmsException e) {
            throw new MessagingException(ErrorEnum.MESSAGINGERROR.getMsg());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param message
     * @return
     */
    public String sendMessageToTopicNotification(final String message) {
        try {
            jmsTemplate.convertAndSend(new ActiveMQTopic(notificationTopicName), message);
            return "message sent.";
        } catch (JmsException e) {
            throw new MessagingException(ErrorEnum.MESSAGINGERROR.getMsg());
        }
    }


}
