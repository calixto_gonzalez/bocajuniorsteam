package ar.com.spring.bocajuniors.bocajuniorsteam.dao;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Data Access Object para buscar informacion sobre los players en la base de datos.
 */
@Repository
public class PlayerDaoSQLImple implements PlayerDaoSQL {
    /**
     * Querys deberian ser declaradas si se van a usar repetidas veces, es por una cuestion de performance.
     */
    private static final String QUERY_SELECT_PLAYER_BY_ID = "select * from players where PLAYER_ID= ?";
    private static final String QUERY_DELETE_PLAYER_BY_ID = "delete from players where PLAYER_ID= ?";
    private static final String QUERY_GET_PLAYERS_BY_COUNTRY = "select * from players where COUNTRY= ?";
    private static final String QUERY_GET_ALL_PLAYERS = "select * from players";
    private static final String QUERY_INSERT_PLAYER = "INSERT INTO PLAYERS(PLAYER_ID,NAME,LASTNAME,COUNTRY,AGE) VALUES (?,?,?,?,?)";

//    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDaoSQLImple.class);

    private JdbcTemplate jdbcTemplate;
    private StatDaoSQLImple statDaoSQLImple;

    /**
     * @param jdbcTemplate {@link JdbcTemplate} conector.
     */
    @Autowired
    public PlayerDaoSQLImple(final JdbcTemplate jdbcTemplate, final StatDaoSQLImple statDaoSQLImple) {
        this.jdbcTemplate = jdbcTemplate;
        this.statDaoSQLImple=statDaoSQLImple;

    }


    /**
     * @return List {@link List} List con Players.
     * @throws DBException {@link DBException}
     */
    @Override
    public List<Player> getAllPlayers() throws DBException {
        try {
            return (List<Player>) jdbcTemplate.query(QUERY_GET_ALL_PLAYERS, new BeanPropertyRowMapper(Player.class));
        } catch (DataAccessException dbe) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg(), dbe);


        }

    }

    /**
     * @param playerRequest {@link ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest} esta clase contiene el id del player a buscar.
     * @return Player {@link Player} retorna un Player segun Id.
     * @throws DBException {@link DBException}
     */
    @Override
    public Player getPlayerById(final int playerRequest) throws DBException {
//
//        return new QueryManager<Player>().executeQueryForObject(jdbcTemplate,QUERY_SELECT_PLAYER_BY_ID,
//                new Object[]{playerRequest},
//                new PlayerMapper(),
//                new Object() {}.getClass().getEnclosingMethod().getName());
        try {
            return (Player) jdbcTemplate.queryForObject(QUERY_SELECT_PLAYER_BY_ID,
                    new Object[]{playerRequest},
                    new BeanPropertyRowMapper(Player.class));

        } catch (EmptyResultDataAccessException erdae) {
            throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());

        } catch (DataAccessException dae) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg());
        }

    }

    /**
     * @param country {@link String}
     * @return List {@link List} List of players by country.
     * @throws DBException {@link DBException}
     */
    @Override
    public List<Player> getPlayersByCountry(final String country) throws DBException {
//
//        return new QueryManager<Player>().executeQuery(jdbcTemplate,QUERY_GET_PLAYERS_BY_COUNTRY,
//                new Object[]{country},
//                new PlayerMapper(),
//                new Object() {}.getClass().getEnclosingMethod().getName());
        try {
            List<Player> players = (List<Player>) jdbcTemplate.query(QUERY_GET_PLAYERS_BY_COUNTRY,
                    new Object[]{country},
                    new BeanPropertyRowMapper(Player.class));
            if (players.isEmpty()) {


                throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());

            } else {
                return players;
            }
        }  catch (DataAccessException dae) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg(), dae);


        }

    }

    @Override
    public void deletePlayerById(final int playerId) throws DBException {
        try {
            getPlayerById(playerId);
            statDaoSQLImple.deleteStatById(playerId);
            jdbcTemplate.update(QUERY_DELETE_PLAYER_BY_ID,
                    new Object[]{playerId});

        } catch (EmptyResultDataAccessException erdae) {
            throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());

        } catch (DataAccessException dae) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg());
        }
    }

    @Override
    public void insertPlayer(final Player player) throws DBException {
        try {
            jdbcTemplate.update(QUERY_INSERT_PLAYER,
                    new Object[]{player.getPlayerId(),player.getName(),player.getLastname(),player.getCountry(),player.getAge()});
        } catch (DuplicateKeyException e) {
            throw new DBException(ErrorEnum.DUPLICATEKEY.getMsg(),e);
        }catch (DataAccessException e) {
            throw new DBException(ErrorEnum.DATABASEERROR.getMsg(),e);
        }
    }

}
