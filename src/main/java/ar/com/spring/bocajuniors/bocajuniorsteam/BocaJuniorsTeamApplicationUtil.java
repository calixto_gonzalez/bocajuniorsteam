package ar.com.spring.bocajuniors.bocajuniorsteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * App
 */
@SuppressWarnings("ALL")
@SpringBootApplication
@EnableScheduling
public class BocaJuniorsTeamApplicationUtil {
    /**
     * @param args command line parameters
     */
    public static void main(final String[] args) {
        SpringApplication.run(BocaJuniorsTeamApplicationUtil.class, args);
    }
}
