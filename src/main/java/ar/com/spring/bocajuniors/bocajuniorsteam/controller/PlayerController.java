package ar.com.spring.bocajuniors.bocajuniorsteam.controller;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.StringException;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.PlayerService;
import ar.com.spring.bocajuniors.bocajuniorsteam.validator.Validator;
import ar.com.spring.bocajuniors.bocajuniorsteam.wrapper.ErrorWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sun.security.pkcs11.Secmod;


/**
 * Este es el controlador de Player que, entre otras cosas, es el que controla como se comporta el micro-servicio.
 */
@RestController
@RequestMapping("/players")
public class PlayerController {
    private Validator validator;
    private PlayerService playerService;

    /**
     *
     * @param playerService {@link PlayerService}
     * @param validator {@link Validator}
     */
    @Autowired
    public PlayerController(final PlayerService playerService, final Validator validator) {
        this.playerService = playerService;

        this.validator = validator;
    }

    /**
     * Busca jugadores en base de datos y retorna la lista de los mismos.
     *
     * @return AllPlayers
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllPlayers() {
        try {
            return ResponseEntity.ok(playerService.getAllPlayers());
        } catch (DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    /**
     * Busca player en base de datos apartir del parametro <code>player</code> que es un objeto mappeado por
     * Spring boot que recibio un archivo del tipo .json, este json contiene un <code>playerId</code> que posee el id
     * de dicho jugador.
     *
     * @param player player
     * @return basta basta
     */
    @RequestMapping(value = "/country", method = RequestMethod.POST)
    public ResponseEntity getPlayersByCountry(@RequestBody final Player player) {
        try {
            validator.validateString(player.getCountry());
            return ResponseEntity.ok(playerService.getPlayersByCountry(player.getCountry()));

        } catch (DBException | StringException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    /**
     * Busca players en base de datos apartir del parametro <code>player</code> que es un objeto mappeado por
     * * Spring boot que recibio un archivo del tipo .json, este json contiene un <code>country</code> que posee el country
     * * "id" de  dichos jugadores.
     * Esta es otra manera de manejar exceptions en vez de ser controladas en el controlador las maneja la exception.
     *
     * @param player {@link Player}
     * @return ResponseEntity {@link ResponseEntity} con la List de Players
     * @throws DBException {@link DBException} lanza Exception.(forma alternativa de agarrar una exception)
     */
    @RequestMapping(value = "/**/country/data", method = RequestMethod.POST)
    public ResponseEntity getPlayersByCountryData(@RequestBody final Player player) throws DBException {
        return ResponseEntity.ok(playerService.getPlayersByCountryData(player.getCountry()));

    }

    /**
     * Busca player en base de datos apartir del parametro <code>playerRequest</code> que es un objeto mappeado por
     * Spring boot que recibio un archivo del tipo .json que contiene <code>idPlayer</code> el id del player.
     *
     * @param playerRequest {@link PlayerRequest} es una clase hecha para mapear de Json a Id(int).
     * @return ResponseEntity {@link ResponseEntity} retorna un player.
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPlayerById(@RequestBody final PlayerRequest playerRequest) {
        try {
            return ResponseEntity.ok(playerService.getPlayerById(playerRequest.getIdPlayer()));

        } catch (StringException | DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     *
     * @param player
     * @return
     */
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public ResponseEntity deletePlayerById(@RequestBody final Player player) {
        try {
            if (player.getPlayerId()==0) {
                throw new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());
            }
            playerService.deletePlayerById(player.getPlayerId());
            return new ResponseEntity(HttpStatus.ACCEPTED);
        } catch (DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()),HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
