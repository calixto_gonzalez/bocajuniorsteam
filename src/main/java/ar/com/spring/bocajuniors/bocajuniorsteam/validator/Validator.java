package ar.com.spring.bocajuniors.bocajuniorsteam.validator;

import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.StringException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Validator class created to validate {@link String} coming from the controller.
 */
@Component
public class Validator {

    private Countries countries;

    /**
     *
     * @param countriesMap {@link Countries}
     */
    @Autowired
    public Validator(final Countries countriesMap) {
        this.countries = countriesMap;
    }

    /**
     * Validates {@link String}
     * @param country {@link String}
     */
    public void validateString(final String country) {
        try {
            Validate.notBlank(country, ErrorEnum.BLANK.getMsg());

        }
        //no es necesario encapsular con expection propia.
        catch (IllegalArgumentException | NullPointerException e) {
            throw new StringException(e.getMessage());
        }
        if (!StringUtils.isAlpha(country)) {
            throw new StringException(ErrorEnum.NOTLETTERS.getMsg());
        }
        if (!countries.isValid(country)) {
            throw new StringException(ErrorEnum.NOTACOUNTRY.getMsg());
        }


    }

}
