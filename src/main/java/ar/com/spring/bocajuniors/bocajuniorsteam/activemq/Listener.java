package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Message;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

//Todo vamos a hacer un unico listener este se va a encargar de mandar los mensajes a la base de datos

/**
 * Listener, as the word suggests listens either a {@link java.util.Queue} or subscribes and listens to
 * a {@link javax.jms.Topic}
 */
@Component
public class Listener {

    private ObjectMapper mapper;

    /**
     * Consructor for Listener
     *
     * @param mapper {@link ObjectMapper}
     */
    @Autowired
    public Listener(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(Listener.class);

    /**
     * Listener for Active MQ destination
     * {@link JmsListener} destination defines where to listen to and containerFactory if it is either a queue or a
     * topic.
     *
     * @param message the message
     */
    @JmsListener(destination = "hola.queue", containerFactory = "queueListenerFactory")
    public void receiveMessage(final String message) {

        LOGGER.debug("Received message  1: {}", message);
        try {
            Stat stat = mapper.readValue(message, Stat.class);
            LOGGER.debug(stat.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * JmsListener {@link JmsListener}
     * @param message
     */
    @JmsListener(destination = "notification", containerFactory = "topicListenerFactory")
    public void notificationTopicReceiver(final String message) {

        LOGGER.debug("Received message: {}", message);


    }

    @JmsListener(destination = "players", containerFactory = "queueListenerFactory")
    public void receiveMessageFromPlayersQueue(final String message) {

        LOGGER.debug("Received message  1: {}", message);
        try {
            Player message1 = mapper.readValue(message, Player.class);
            LOGGER.debug(message1.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO arreglar todo basicamente


    }
    @JmsListener(destination = "stats", containerFactory = "queueListenerFactory")
    public void receiveMessageFromStatsQueue(final String message) {

        LOGGER.debug("Received message  1: {}", message);
        try {
            Stat stat = mapper.readValue(message, Stat.class);
            LOGGER.debug(stat.toString());


        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO arreglar todo basicamente


    }

    @JmsListener(destination = "messages", containerFactory = "queueListenerFactory")
    public void receiveMessageFromMessagesQueue(final String message) {

        LOGGER.debug("Received message  1: {}", message);
        //TODO arreglar todo basicamente


    }

//    @JmsListener(destination ="hola.queue" ,containerFactory = "queueListenerFactory")
//    public void receiveMessage2(final String message) {
//
//        LOGGER.info("Received message   2: {}", message);
//        try {
//            Stat student=mapper.readValue(message,Stat.class);
//
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


}


