package ar.com.spring.bocajuniors.bocajuniorsteam.validator;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Countries is a class created to communicate with a RESTApi web service that provides all the countries in the world
 * it's use for validation purposes.
 */
@Component
public class Countries {
    private RestTemplate restTemplate = new RestTemplate();
    private List<Object> list;
    private Map<String, String> result = new HashMap<>();

    /**
     * {@link PostConstruct} as the name suggests Spring boot uses this annotation to execute this method just after
     * the project has launched.
     */
    @PostConstruct
    public void init() {
        ResponseEntity<Object[]> responseEntity = restTemplate
                .getForEntity("https://restcountries.eu/rest/v2/all?fields=name;alpha3Code", Object[].class);

            list = new ArrayList(Arrays.asList(responseEntity.getBody()));

            list.stream().forEach(map -> {
                result.put(((Map<String, String>) map).get("name"), ((Map<String, String>) map).get("alpha3Code"));
            });

    }

    /**
     * @return Map {@link Map} with String,String (code and country it belongs to)
     */
    public Map<String, String> getResult() {
        return result;
    }

    /**
     * @param country {@link String}
     * @return boolean boolean
     */
    public boolean isValid(final String country) {
        return result.containsKey(country);

    }
}
