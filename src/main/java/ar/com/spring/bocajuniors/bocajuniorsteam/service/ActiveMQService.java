package ar.com.spring.bocajuniors.bocajuniorsteam.service;


import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Publisher;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * ActiveMQService
 */
@Service
public class ActiveMQService {
    private StatDaoSQLImple statDao;
    private Publisher publisher;

    /**
     * @param publisher {@link Publisher}
     */
    @Autowired
    public ActiveMQService(final Publisher publisher, StatDaoSQLImple statDao) {
        this.statDao = statDao;
        this.publisher = publisher;
    }

    /**
     * @param s {@link String}
     * @throws Exception {@link Exception}
     */
    public void sender(final String s) throws Exception {

        JAXBContext jaxbContext = JAXBContext.newInstance(Stat.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(s);
        Stat stat = (Stat) jaxbUnmarshaller.unmarshal(reader);
        publisher.sender(stat);

        statDao.insertStat(stat);


    }

}
