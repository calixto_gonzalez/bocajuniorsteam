package ar.com.spring.bocajuniors.bocajuniorsteam.exception;

public class MessagingException extends RuntimeException {

    public MessagingException(final String s) {
        super(s);
    }

    public MessagingException(final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
