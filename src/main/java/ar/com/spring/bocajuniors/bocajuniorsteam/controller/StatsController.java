package ar.com.spring.bocajuniors.bocajuniorsteam.controller;

import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.StatService;
import ar.com.spring.bocajuniors.bocajuniorsteam.wrapper.ErrorWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Este es el controlador de Stats que, entre otras cosas, es el que controla como se comporta el micro-servicio.
 */
@RestController
@RequestMapping("/stats")
public class StatsController {


    private StatService statService;

    /**
     * Inyecta statService en el constructor del Stat controller.
     * @param statService {@link StatService}
     */
    @Autowired
    public StatsController(final StatService statService) {
        this.statService = statService;
    }

    /**
     * Busca las estadisticas de todos los jugadores que estan en la base de datos.
     *
     * @return ResponseEntity {@link ResponseEntity} retorna List de Stats con Players.
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllStats() {
        try {
            return ResponseEntity.ok(statService.getAllStats());

        } catch (DBException e) {
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
