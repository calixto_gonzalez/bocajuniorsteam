package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

public class ListenerTest {
    private Listener listener;
    private Stat statMock;

    private ObjectMapper objectMapperTest;

    @Before
    public void setup() {
        objectMapperTest=Mockito.mock(ObjectMapper.class);
        listener=new Listener(objectMapperTest);
//        listener=new Listener(new ObjectMapper());
//        statMock=Mockito.mock(Stat.class);

    }


    @Test
    public void receiveMessage() throws IOException {
        Stat expectedStat = new Stat();
        expectedStat.setPlayerId(22);

        doReturn(expectedStat).when(objectMapperTest).readValue(anyString(), any(Class.class));

        listener.receiveMessage("{\"idStats\":\"22\"}");

        Mockito.verify(objectMapperTest).readValue(anyString(), any(Class.class));
    }
}