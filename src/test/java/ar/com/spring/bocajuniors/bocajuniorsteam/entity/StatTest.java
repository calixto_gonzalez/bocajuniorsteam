package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class StatTest {
//    private int fakeId;
//    private int fakeIdPlayer;
//    private String fakeRival;
//    private int fakeResult;
//    private int fakeGoals;
//    private int fakeYellows;
//    private int fakeReds;
//    private String fakeCommentary;
//    private Date fakeTime;
    private Stat fakeStat;
    private Stat fakeStatReturned;



    @Before
    public void setup() {
        fakeStat=new Stat(1,1,"0-1",2,0,1,"Jugo maso",new Date(),"Riber");
        fakeStatReturned=new Stat();



    }

    @Test
    public void shouldSetEveryStatField() {
        fakeStatReturned.setIdStats(fakeStat.getIdStats());
        fakeStatReturned.setCommentary(fakeStat.getCommentary());
        fakeStatReturned.setGoals(fakeStat.getGoals());
        fakeStatReturned.setPlayerId(fakeStat.getPlayerId());
        fakeStatReturned.setTime(fakeStat.getTime());
        fakeStatReturned.setReds(fakeStat.getReds());
        fakeStatReturned.setYellows(fakeStat.getYellows());
        fakeStatReturned.setRival(fakeStat.getRival());
        fakeStatReturned.setResult(fakeStat.getResult());
        assertEquals(fakeStat.getIdStats(),fakeStatReturned.getIdStats());
        assertEquals(fakeStat.getCommentary(),fakeStatReturned.getCommentary());
        assertEquals(fakeStat.getGoals(),fakeStatReturned.getGoals());
        assertEquals(fakeStat.getPlayerId(),fakeStatReturned.getPlayerId());
        assertEquals(fakeStat.getTime(),fakeStatReturned.getTime());
        assertEquals(fakeStat.getReds(),fakeStatReturned.getReds());
        assertEquals(fakeStat.getYellows(),fakeStatReturned.getYellows());
        assertEquals(fakeStat.getRival(),fakeStatReturned.getRival());
        assertEquals(fakeStat.getResult(),fakeStatReturned.getResult());
    }
//    @Test
//    public void getAllFields() {
//
//
//    }
//
//    @Test
//    public void getPlayerId() {
//    }
//
//    @Test
//    public void getGoals() {
//    }
//
//    @Test
//    public void getPlayerId() {
//    }
//
//    @Test
//    public void setTime() {
//    }
//
//    @Test
//    public void getRival() {
//    }
//
//    @Test
//    public void setResult() {
//    }
//
//    @Test
//    public void getReds() {
//    }
//
//    @Test
//    public void setRival() {
//    }
//
//    @Test
//    public void getCommentary() {
//    }
//
//    @Test
//    public void setCommentary() {
//    }
//
//    @Test
//    public void setGoals() {
//    }
//
//    @Test
//    public void setReds() {
//    }
//
//    @Test
//    public void getTime() {
//    }
//
//    @Test
//    public void setYellows() {
//    }
//
//    @Test
//    public void getYellows() {
//    }
//
//    @Test
//    public void setPlayerId() {
//    }
//
//    @Test
//    public void setPlayerId() {
//    }
}