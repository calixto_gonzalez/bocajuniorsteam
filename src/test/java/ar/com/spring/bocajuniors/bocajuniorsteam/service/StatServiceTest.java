package ar.com.spring.bocajuniors.bocajuniorsteam.service;

import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQL;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader.PlayerLoader;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class StatServiceTest {
//    @Rule
////    public ExpectedException thrown=ExpectedException.none();
////
////    PlayerDaoSQLImple playerDao;
////    StatDaoSQLImple statDao;
////    PlayerService playerService;
////    List<Player> playerListFakePlayersAndStats;
////    PlayerLoader playerLoader;
////    List<Player> playerListFakePlayers;
////
////
////    @Before
////    public void setup() {
////        playerDao = Mockito.mock(PlayerDaoSQLImple.class);
////        statDao = Mockito.mock(StatDaoSQLImple.class);
////        playerLoader = Mockito.mock(PlayerLoader.class);
////        playerService = new PlayerService(playerDao, statDao, playerLoader);
////        //Siempre se mockea la implementacion y no la interface
////        playerListFakePlayersAndStats = Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60,Arrays.asList(new Stat(1,1,1,1,1,1,"hola",new Date(),"Portugal"))));
////        playerListFakePlayers=Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60));
////
////    }
    @Rule
    public ExpectedException thrown=ExpectedException.none();

    StatService statService;
    StatDaoSQLImple statDaoSQLImple;
    PlayerDaoSQL playerDaoSQLImple;
    PlayerLoader playerLoader;
    List<Player> playerListWithStatsFromService;
    List<Player> playerListWithStatsFromDao;


    @Before
    public void setup() {
        statDaoSQLImple=Mockito.mock(StatDaoSQLImple.class);
        playerDaoSQLImple=Mockito.mock(PlayerDaoSQLImple.class);
        playerLoader=Mockito.mock(PlayerLoader.class);
        statService=new StatService(playerDaoSQLImple,playerLoader);
        playerListWithStatsFromService = Arrays.asList(new Player(1, "sarasa", "lopez", "peru", 60,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))),
                new Player(1, "Cristian", "Pavon", "Argentina", 22,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))));
        playerListWithStatsFromDao = Arrays.asList(new Player(1, "sarasa", "lopez", "peru", 60,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))),
                new Player(1, "Cristian", "Pavon", "Argentina", 22,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))));

    }



    @Test
    public void getAllStatsShouldGetPlayersWithStats() throws DBException {

        Mockito.doReturn(playerListWithStatsFromDao).when(playerDaoSQLImple).getAllPlayers();
        Mockito.doReturn(playerListWithStatsFromService).when(playerLoader).loadStats(playerListWithStatsFromDao);
        List<Player> playerListReturned=statService.getAllStats();
        assertEquals(playerListWithStatsFromService,playerListReturned);
    }

    @Test
    public void getAllStatsShouldThrowDBException() throws DBException {

//        final DBException dbException=new DBException("Any error");
//        thrown.expect(DBException.class);
//        thrown.expectMessage("Any error");
//
//
//        Mockito.doThrow(dbException).when(playerDao).getPlayersByCountry(Mockito.anyString());
//        Mockito.doThrow(dbException).when(playerLoader).loadStats(playerListFakePlayers);
//        //IMPORTANT PREGUNTAR
//
//        playerService.getPlayersByCountry(playerListFakePlayers.get(0).getCountry());
//        playerLoader.loadStats(playerListFakePlayers);
        thrown.expect(DBException.class);
        thrown.expectMessage("Any Error");

        final DBException dbException=new DBException("Any Error");

        Mockito.doThrow(dbException).when(playerDaoSQLImple).getAllPlayers();
        Mockito.doThrow(dbException).when(playerLoader).loadStats(playerListWithStatsFromDao);
        statService.getAllStats();


    }
}