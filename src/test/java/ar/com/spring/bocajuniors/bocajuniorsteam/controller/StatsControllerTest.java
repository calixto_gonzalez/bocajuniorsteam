package ar.com.spring.bocajuniors.bocajuniorsteam.controller;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.PlayerService;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.StatService;
import ar.com.spring.bocajuniors.bocajuniorsteam.wrapper.ErrorWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class StatsControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private StatService statService;
    private StatsController statsController;


    @Before
    public void setup() {
        statService = Mockito.mock(StatService.class);
        statsController = new StatsController(statService);





    }

    @Test
    public void getAllStatsShouldGetPlayerWithStats() throws DBException, ParseException {

        //    return new PlayerLoader().loadStats(playerDaoSQLM.getAllPlayers(),statDao);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = sdf.parse("13-11-1993");
        List<Player> playerList = Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60,
                Arrays.asList(new Stat(1,1,"0-1",1,1,1,"fiero",date,"Francia"))));
        Mockito.doReturn(playerList).when(statService).getAllStats();

        ResponseEntity responseEntity = statsController.getAllStats();
        List<Player> playerListTest = (List<Player>) responseEntity.getBody();
        Assert.assertNotNull(playerListTest);
        Assert.assertTrue(playerListTest.size() == 1);
        Assert.assertEquals(playerListTest.get(0).getName(), playerList.get(0).getName());

    }
    @Test
    public void getAllStatsShouldThrowException() throws DBException {
        final DBException dbException = new DBException("Any error");

        Mockito.doThrow(dbException).when(statService).getAllStats();
        ResponseEntity responseEntity = statsController.getAllStats();// compara error
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();
        assertEquals(responseEntity.getStatusCode(),HttpStatus.INTERNAL_SERVER_ERROR);
        assertNotNull(errorWrapper);
        assertEquals(dbException.getMessage(), errorWrapper.getMessage());
        assertEquals(responseEntity.getStatusCodeValue(),HttpStatus.INTERNAL_SERVER_ERROR.value()); // compara valor del error

    }
}