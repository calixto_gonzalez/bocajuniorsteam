package ar.com.spring.bocajuniors.bocajuniorsteam.controller;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.service.PlayerService;
import ar.com.spring.bocajuniors.bocajuniorsteam.validator.Countries;
import ar.com.spring.bocajuniors.bocajuniorsteam.validator.Validator;
import ar.com.spring.bocajuniors.bocajuniorsteam.wrapper.ErrorWrapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class PlayerControllerTest {
    @Rule
    public ExpectedException thrown=ExpectedException.none();

    private PlayerController playerController;
    private PlayerService playerService;
    private Countries countriesValidatorTest;
    private Validator validatorTest;

    @Before
    public void setup() {
        playerService = Mockito.mock(PlayerService.class);
        validatorTest=Mockito.mock(Validator.class);
        countriesValidatorTest=Mockito.mock(Countries.class);
        playerController = new PlayerController(playerService,validatorTest);
    }


    @Test
    public void getAllPlayersShouldReturnAListOfPlayers() throws DBException {
        List<Player> playerList = Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60));
        Mockito.doReturn(playerList).when(playerService).getAllPlayers();
        ResponseEntity responseEntity = playerController.getAllPlayers();
        List<Player> playerListTest = (List<Player>) responseEntity.getBody();
        Assert.assertNotNull(playerListTest);
        Assert.assertTrue(playerListTest.size() == 1);
        Assert.assertEquals(playerListTest.get(0).getName(), playerList.get(0).getName());


    }

    @Test
    public void getAllPlayersShouldThrowExceptionWhenPlayerServiceFails() throws DBException {

        final DBException dbException = new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        Mockito.doThrow(dbException).when(playerService).getAllPlayers();

        ResponseEntity responseEntity = playerController.getAllPlayers();// compara error
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();
        assertEquals(responseEntity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertNotNull(errorWrapper);
        assertEquals(dbException.getMessage(), errorWrapper.getMessage());
        assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.INTERNAL_SERVER_ERROR.value()); // compara valor del error


    }


    @Test
    public void getPlayerByIdShouldReturnPlayer() throws DBException {
        final Player player = new Player(1, "Cristian", "Pavon", "Argentina", 22);
        final PlayerRequest playerRequest = new PlayerRequest(player.getPlayerId());
        Mockito.doReturn(player).when(playerService).getPlayerById(Mockito.anyInt());


        ResponseEntity responseEntity = playerController.getPlayerById(playerRequest);
        Player playerTest = (Player) responseEntity.getBody();

        assertNotNull(playerTest);
        assertEquals(playerTest.getPlayerId(), player.getPlayerId());
        assertEquals(playerTest.getPlayerId(), playerRequest.getIdPlayer());
        assertEquals(playerTest.getName(), player.getName());


    }

    /**
     * @throws DBException
     */
    @Test
    public void getPlayerByIdThrowExceptionWhenPlayerServiceFails() throws DBException {

        final PlayerRequest playerRequest = new PlayerRequest();
        final DBException dbException = new DBException(ErrorEnum.USERNOTFOUNDERROR.getMsg());

        Mockito.doThrow(dbException).when(playerService).getPlayerById(Mockito.anyInt());


        ResponseEntity responseEntity = playerController.getPlayerById(playerRequest);
        ErrorWrapper errorWrapper = (ErrorWrapper) responseEntity.getBody();

        assertEquals(responseEntity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertNotNull(errorWrapper);
        assertEquals(dbException.getMessage(), errorWrapper.getMessage());
        assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.INTERNAL_SERVER_ERROR.value());


    }

    @Test
    public void getPlayersByCountryShouldGetListOfPlayerByCountry() throws DBException {

        List<Player> playerList = Arrays.asList(new Player(1, "sarasa", "lopez", "peru", 60,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))),
                new Player(1, "Cristian", "Pavon", "Argentina", 22,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))));
        Mockito.doReturn(playerList).when(playerService).getPlayersByCountry(Mockito.anyString());
        ResponseEntity responseEntity = playerController.getPlayersByCountry(new Player(0, null, null, "bolivia", 0, null));
        List<Player> playerListTest = (List<Player>) responseEntity.getBody();
        Assert.assertEquals(playerList, playerListTest);
    }

    @Test
    public void getPlayerByCountryShouldThrowDBException() throws DBException {

        final DBException dbException = new DBException("Any Error");
        Mockito.doThrow(dbException).
                when(playerService).getPlayersByCountry(Mockito.anyString());
        ResponseEntity responseEntity = playerController.getPlayersByCountry(new Player(0, null, null, "something not valid", 0, null));
        assertEquals(responseEntity.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);

    }


    @Test
    public void getPlayersByCountryDataShouldGetPlayersByCountryToo() throws DBException{
        List<Player> playerList = Arrays.asList(new Player(1, "sarasa", "lopez", "peru", 60,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))),
                new Player(1, "Cristian", "Pavon", "Argentina", 22,
                        Arrays.asList(new Stat(1, 1, "0-1", 1, 1, 1, "fiero", new Date(), "Francia"))));
        Mockito.doReturn(playerList).when(playerService).getPlayersByCountryData(Mockito.anyString());
        ResponseEntity responseEntity = playerController.getPlayersByCountryData(new Player(0, null, null, "bolivia", 0, null));
        List<Player> playerListTest = (List<Player>) responseEntity.getBody();
        Assert.assertEquals(playerList, playerListTest);



    }
    @Test
    public void getPlayerByCountryDataShouldThrowDBException() throws DBException {
        final DBException dbException = new DBException("Any Error");
        thrown.expect(DBException.class);
        thrown.expectMessage("Any Error");
        Mockito.doThrow(dbException).
                when(playerService).getPlayersByCountryData(Mockito.anyString());
        playerController.getPlayersByCountryData(new Player(0, null, null, "something not valid", 0, null));


    }
}