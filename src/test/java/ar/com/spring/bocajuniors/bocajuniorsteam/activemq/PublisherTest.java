package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Queue;
import javax.jms.Topic;
import java.util.Date;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;

public class PublisherTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private JmsTemplate jmsTemplateMock;
    private Queue queueMock;
    private Topic topicMock;
    private ObjectMapper objectMapperMock;
    private Publisher publisherTest;
    private Stat statMock;


    @Before
    public void setup() {

        jmsTemplateMock = Mockito.mock(JmsTemplate.class);
        queueMock = Mockito.mock(Queue.class);
        topicMock = Mockito.mock(Topic.class);
        objectMapperMock = Mockito.mock(ObjectMapper.class);

        publisherTest = new Publisher(jmsTemplateMock, queueMock, topicMock, objectMapperMock);
        statMock=Mockito.mock(Stat.class);
    }


    @Test
    public void senderShouldSend() {

//        doNothing().when(jmsTemplateMock).convertAndSend(anyString());
        publisherTest.sender(statMock);

    }
    @Test
    public void senderShouldSendNull() {

        doNothing().when(jmsTemplateMock).convertAndSend(anyString());
        publisherTest.sender(null);

    }
    @Test
    public void senderShouldSendEmpty() {

        doNothing().when(jmsTemplateMock).convertAndSend(anyString());
        publisherTest.sender(new Stat());

    }
    @Test
    public void senderShouldThrowException() throws JsonProcessingException {
        doThrow(JsonProcessingException.class).when(objectMapperMock).writeValueAsString(statMock);
        publisherTest.sender(statMock);
        Mockito.verify(objectMapperMock,times(1)).writeValueAsString(statMock);
        Mockito.verify(jmsTemplateMock,times(0)).convertAndSend(anyString());

    }
}
