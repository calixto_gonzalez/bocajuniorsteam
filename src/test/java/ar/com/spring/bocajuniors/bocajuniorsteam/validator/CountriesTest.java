package ar.com.spring.bocajuniors.bocajuniorsteam.validator;

import ar.com.spring.bocajuniors.bocajuniorsteam.mockito2.CustomWhitebox;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;

public class CountriesTest {

    private RestTemplate restTemplate;
    private Object[] objectsTest;
    private ArrayList<Object> listTest;
    private Countries countries;
    private Map<String,String> mapTest= new HashMap<>();
    private ResponseEntity<Object[]> responseEntity;
    private LinkedHashMap<String,String> linkedHashMapTest;

    @Before
    public void setup() {
        this.restTemplate= Mockito.mock(RestTemplate.class);
        this.responseEntity=Mockito.mock(ResponseEntity.class);

        doReturn(getBodyForEntity()).when(responseEntity).getBody();
        doReturn(responseEntity).when(restTemplate).getForEntity(anyString(),Mockito.any());

        this.countries=new Countries();
        CustomWhitebox.setInternalState(countries,"restTemplate",restTemplate);
        countries.init();
        mapTest=countries.getResult();

    }

    @Test
    public void init() {
        countries.init();
    }

    @Test
    public void getResultShouldReturnAMap() {
        assertNotNull(countries.getResult());


    }

    @Test
    public void isValidShouldReturnTrue() {
        assertTrue(countries.isValid("Argentina"));
    }
    @Test
    public void isValidShouldReturnFalse() {
        assertFalse(countries.isValid(""));
    }

    private Object[] getBodyForEntity() {
        objectsTest= new Object[1];
        linkedHashMapTest= new LinkedHashMap<>();
        linkedHashMapTest.put("name","Argentina");
        linkedHashMapTest.put("alpha3Code","ARG");
        objectsTest[0] = linkedHashMapTest;

        return objectsTest;
    }
}