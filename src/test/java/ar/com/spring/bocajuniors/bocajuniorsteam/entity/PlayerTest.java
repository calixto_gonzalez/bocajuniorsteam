package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

public class PlayerTest {

    private Player fakePlayer;
    private int fakeId;
    private String fakeName;
    private String fakeLastname;
    private String fakeCountry;
    private int fakeAge;
    private List<Stat> fakeStatList;
    private Player fakePlayerReturned;
    private Player fakePlayerWithoutList;




    @Before
    public void setup(){
        this.fakePlayer =new Player(1,"Carlitos","Tevez","Argentina",34,Collections.emptyList());
        this.fakeId=fakePlayer.getPlayerId();
        this.fakeName=fakePlayer.getName();
        this.fakeLastname=fakePlayer.getLastname();
        this.fakeCountry=fakePlayer.getCountry();
        this.fakeAge=fakePlayer.getAge();
        this.fakeStatList=fakePlayer.getStatList();
        this.fakePlayerReturned=new Player();
        this.fakePlayerWithoutList=new Player(1,"Carlitos","Tevez","Argentina",34);

    }

    @Test
    public void getName() {
        doReturn(fakeName).when(Mockito.mock(Player.class)).getName();
        String fakeNameReturned=fakePlayer.getName();
        assertEquals(fakeName,fakeNameReturned);

    }

    @Test
    public void setName() {
        doNothing().when(Mockito.mock(Player.class)).setName(fakeName);
        fakePlayerReturned.setName(fakeName);
        assertEquals(fakeName,fakePlayerReturned.getName());
    }

    @Test
    public void getLastname() {
        doReturn(fakeLastname).when(Mockito.mock(Player.class)).getLastname();
        String fakeNameLastname=fakePlayer.getLastname();
        assertEquals(fakeLastname,fakeNameLastname);
    }

    @Test
    public void setLastname() {
        doNothing().when(Mockito.mock(Player.class)).setLastname(fakeLastname);
        fakePlayerReturned.setLastname(fakeLastname);
        assertEquals(fakeLastname,fakePlayerReturned.getLastname());
    }

    @Test
    public void getCountry() {
        doReturn(fakeCountry).when(Mockito.mock(Player.class)).getCountry();
        String fakeCountryReturned=fakePlayer.getCountry();
        assertEquals(fakeCountry,fakeCountryReturned);
    }

    @Test
    public void setCountry() {
        doNothing().when(Mockito.mock(Player.class)).setCountry(fakeCountry);
        fakePlayerReturned.setCountry(fakeCountry);
        assertEquals(fakeCountry,fakePlayerReturned.getCountry());
    }

    @Test
    public void getId() {
        doReturn(fakeId).when(Mockito.mock(Player.class)).getPlayerId();
        int fakeIdReturned=fakePlayer.getPlayerId();
        assertEquals(fakeId,fakeIdReturned);
    }

    @Test
    public void setId() {
        doNothing().when(Mockito.mock(Player.class)).setPlayerId(fakeId);
        fakePlayerReturned.setPlayerId(fakeId);
        assertEquals(fakeId,fakePlayerReturned.getPlayerId());
    }

    @Test
    public void getAge() {
        doReturn(fakeAge).when(Mockito.mock(Player.class)).getAge();
        int fakeAgeReturned=fakePlayer.getAge();
        assertEquals(fakeAge,fakeAgeReturned);
    }

    @Test
    public void setAge() {
        doNothing().when(Mockito.mock(Player.class)).setAge(fakeAge);
        fakePlayerReturned.setAge(fakeAge);
        assertEquals(fakeAge,fakePlayerReturned.getAge());
    }

    @Test
    public void getStatList() {
        doReturn(fakeStatList).when(Mockito.mock(Player.class)).getStatList();
        List<Stat> fakeListReturned=fakePlayer.getStatList();
        assertEquals(fakeStatList.isEmpty(),fakeListReturned.isEmpty());
    }

    @Test
    public void setStatList() {
        doNothing().when(Mockito.mock(Player.class)).setStatList(fakeStatList);
        fakePlayerReturned.setStatList(fakeStatList);
        assertEquals(fakeStatList.isEmpty(),fakePlayerReturned.getStatList().isEmpty());
    }
   @Test
    public void TestObjectsEquality() {
        fakePlayerReturned.setName(fakeName);
        assertEquals(fakePlayerWithoutList.getName(),fakePlayerReturned.getName());
        fakePlayerReturned.setStatList(null);
       assertEquals(fakePlayerWithoutList.getStatList(),fakePlayerReturned.getStatList());


   }
}