package ar.com.spring.bocajuniors.bocajuniorsteam.service;

import ar.com.spring.bocajuniors.bocajuniorsteam.activemq.Publisher;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.PlayerDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader.PlayerLoader;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class PlayerServiceTest {

    @Rule
    public ExpectedException thrown=ExpectedException.none();

    PlayerDaoSQLImple playerDao;
    StatDaoSQLImple statDao;
    PlayerService playerService;
    List<Player> playerListFakePlayersAndStats;
    PlayerLoader playerLoader;
    List<Player> playerListFakePlayers;
    Publisher publisher;


    @Before
    public void setup() {
        playerDao = Mockito.mock(PlayerDaoSQLImple.class);
        statDao = Mockito.mock(StatDaoSQLImple.class);
        playerLoader = Mockito.mock(PlayerLoader.class);
        publisher=Mockito.mock(Publisher.class);
        playerService = new PlayerService(playerDao, playerLoader,publisher);
        //Siempre se mockea la implementacion y no la interface
        playerListFakePlayersAndStats = Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60,Arrays.asList(new Stat(1,1,"0-1",1,1,1,"hola",new Date(),"Portugal"))));
        playerListFakePlayers=Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60));

    }

    @Test
    public void getAllPlayersShouldGetAllPlayers() throws DBException {
        Mockito.doReturn(playerListFakePlayers).when(playerDao).getAllPlayers();
        List<Player> playerListFakePlayersReturned=playerService.getAllPlayers();
        assertNotNull(playerListFakePlayersReturned);
        assertEquals(playerListFakePlayers.get(0).getName(),playerListFakePlayersReturned.get(0).getName());
    }

    @Test
    public void getAllPlayersShouldThrowDBException() throws DBException {
        final DBException dbException=new DBException(ErrorEnum.DATABASEERROR.getMsg());
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.DATABASEERROR.getMsg());
        Mockito.doThrow(dbException).when(playerDao).getAllPlayers();
        playerService.getAllPlayers();

    }

    @Test
    public void getPlayerByIdShouldGetPlayer() throws DBException {
        //return playerDao.getPlayerById(idPlayer);
        Mockito.doReturn(playerListFakePlayers.get(0)).when(playerDao).getPlayerById(Mockito.anyInt());
        Player playerFakeReturn=playerService.getPlayerById(playerListFakePlayers.get(0).getPlayerId());
        assertNotNull(playerFakeReturn);
        assertEquals(playerListFakePlayers.get(0).getName(),playerFakeReturn.getName());


    }
    @Test
    public void getPlayerByIdShouldThrowDBException() throws DBException {
        final DBException dbException=new DBException("Any error");
        thrown.expect(DBException.class);
        thrown.expectMessage("Any error");
        Mockito.doThrow(dbException).when(playerDao).getPlayerById(Mockito.anyInt());
        playerService.getPlayerById(-2);

    }

    @Test
    public void getPlayersByCountryShouldGetPlayer() throws DBException {
        Mockito.doReturn(playerListFakePlayers).when(playerDao).getPlayersByCountry(Mockito.anyString());
        List<Player> playerListFakePlayersReturned=playerService.getPlayersByCountry(playerListFakePlayers.get(0).getCountry());
        assertNotNull(playerListFakePlayersReturned);
        assertEquals(playerListFakePlayers.size(),playerListFakePlayersReturned.size());
        assertEquals(playerListFakePlayers.get(0).getName(),playerListFakePlayersReturned.get(0).getName());

    }
    @Test
    public void getPlayersByCountryShouldThrowDBException() throws DBException {
        final DBException dbException=new DBException("Any error");
        thrown.expect(DBException.class);
        thrown.expectMessage("Any error");
        Mockito.doThrow(dbException).when(playerDao).getPlayersByCountry(Mockito.anyString());
        playerDao.getPlayersByCountry(playerListFakePlayers.get(0).getCountry());

    }


    @Test
    public void getPlayersByCountryDataShouldGetPlayersAndStats() throws DBException {
        Mockito.doReturn(playerListFakePlayers).when(playerDao).getPlayersByCountry(Mockito.anyString());
        Mockito.doReturn(playerListFakePlayersAndStats).when(playerLoader).loadStats(playerListFakePlayers);
        List<Player> playerListFakePlayersReturned = playerService.getPlayersByCountryData(playerListFakePlayers.get(0).getCountry());
        List<Player> playerListFakePlayersAndStatsReturned = playerLoader.loadStats(playerListFakePlayers);
        assertEquals(playerListFakePlayers.size(),playerListFakePlayersReturned.size());
        assertEquals(playerListFakePlayersAndStats.size(),playerListFakePlayersAndStatsReturned.size());
    }
    @Test
    public void getPlayersByCountryDataShouldThrowDBException() throws DBException {
        final DBException dbException=new DBException("Any error");
        thrown.expect(DBException.class);
        thrown.expectMessage("Any error");


        Mockito.doThrow(dbException).when(playerDao).getPlayersByCountry(Mockito.anyString());
        Mockito.doThrow(dbException).when(playerLoader).loadStats(playerListFakePlayers);
        //IMPORTANT PREGUNTAR

        playerService.getPlayersByCountry(playerListFakePlayers.get(0).getCountry());
        playerLoader.loadStats(playerListFakePlayers);

    }
}