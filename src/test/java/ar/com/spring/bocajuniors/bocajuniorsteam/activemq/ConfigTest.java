package ar.com.spring.bocajuniors.bocajuniorsteam.activemq;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

import javax.jms.ConnectionFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

public class ConfigTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Config configTest;

    private ConnectionFactory connectionFactory;
    private DefaultJmsListenerContainerFactoryConfigurer configurer;

    @Before
    public void setUp() {
        connectionFactory = mock(ConnectionFactory.class);
        configurer = mock(DefaultJmsListenerContainerFactoryConfigurer.class);

    }

    @Test
    public void queueListenerFactory() {
        doNothing().when(configurer).configure(any(),any());

        configTest = new Config("someTopic", "someQueue");
        JmsListenerContainerFactory<?> result = configTest.queueListenerFactory(connectionFactory, configurer);
        assertNotNull(result);
        assertEquals(result instanceof DefaultJmsListenerContainerFactory, true);
    }

    @Test
    public void queueListenerFactoryWihNullParametersShouldThrowNullPointerException() {
        thrown.expect(NullPointerException.class);

        configTest = new Config("someTopic", "someQueue");
        configTest.queueListenerFactory(null, null);
    }

    @Test
    public void queueListenerFactoryWihNullConfigurerShouldThrowNullPointerException() {
        thrown.expect(NullPointerException.class);

        configTest = new Config("someTopic", "someQueue");
        configTest.queueListenerFactory(connectionFactory, null);
    }

    @Test
    public void queueListenerFactoryWihNullConnectionFactoryShouldReturnDefaultJmsListenerContainerFactory() {
        doNothing().when(configurer).configure(any(),any());
        configTest = new Config("someTopic", "someQueue");
        JmsListenerContainerFactory<?> result = configTest.queueListenerFactory(null, configurer);
        assertNotNull(result);
        assertEquals(result instanceof DefaultJmsListenerContainerFactory, true);
    }
    //TODO
    @Test
    public void topicListenerFactory() {
        doNothing().when(configurer).configure(any(),any());

        configTest = new Config("someTopic", "someQueue");
        JmsListenerContainerFactory<?> result = configTest.topicListenerFactory(connectionFactory, configurer);
        assertNotNull(result);
        assertEquals(result instanceof DefaultJmsListenerContainerFactory, true);
    }

    @Test
    public void topicListenerFactoryWihNullParametersShouldThrowNullPointerException() {
        thrown.expect(NullPointerException.class);

        configTest = new Config("someTopic", "someQueue");
        configTest.topicListenerFactory(null, null);
    }

    @Test
    public void topicListenerFactoryWihNullConfigurerShouldThrowNullPointerException() {
        thrown.expect(NullPointerException.class);

        configTest = new Config("someTopic", "someQueue");
        configTest.topicListenerFactory(connectionFactory, null);
    }

    @Test
    public void topicListenerFactoryWihNullConnectionFactoryShouldReturnDefaultJmsListenerContainerFactory() {
        doNothing().when(configurer).configure(any(),any());
        configTest = new Config("someTopic", "someQueue");
        JmsListenerContainerFactory<?> result = configTest.topicListenerFactory(null, configurer);
        assertNotNull(result);
        assertEquals(result instanceof DefaultJmsListenerContainerFactory, true);
    }

    @Test
    public void queue() {
    }

    @Test
    public void topic() {
    }
}