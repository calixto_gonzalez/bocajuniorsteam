package ar.com.spring.bocajuniors.bocajuniorsteam.dao;


import ar.com.spring.bocajuniors.bocajuniorsteam.entity.PlayerRequest;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Stat;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatDaoSQLImpleTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private JdbcTemplate jdbcTemplate;
    private StatDaoSQLImple statDaoSQLImple;
    private List<Stat> statListFake;
    private PlayerRequest playerRequestFake;



    @Before
    public void setUp() {
        jdbcTemplate = (Mockito.mock(JdbcTemplate.class));
        statDaoSQLImple = (new StatDaoSQLImple(jdbcTemplate));
        statListFake=Arrays.asList(new Stat(1,1,"0-1",3,0,0,"porfavor no te vayas de boca",new Date(),"Riber"));
        playerRequestFake=new PlayerRequest();
        playerRequestFake.setIdPlayer(statListFake.get(0).getIdStats());
    }

    @Test
    public void getAllStatsByPlayerShouldGetStats() throws DBException {
        Mockito.doReturn(statListFake).when(jdbcTemplate).query(Mockito.anyString(),Mockito.any(BeanPropertyRowMapper.class));
        List<Stat> playerListFakeReturned = statDaoSQLImple.getAllStatsByPlayer(playerRequestFake.getIdPlayer());
        assertEquals(statListFake.size(),playerListFakeReturned.size());

    }
    @Test
    public void getAllStatsByPlayerShouldThrowEmptyResultDataAccessException() throws DBException{
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        Mockito.doThrow(EmptyResultDataAccessException.class).when(jdbcTemplate).query(Mockito.anyString(),Mockito.any(BeanPropertyRowMapper.class));
        List<Stat> pStatListFakeReturned=statDaoSQLImple.getAllStatsByPlayer(0);

    }
    @Test
    public void getAllStatsByPlayerShouldThrowDataAccessException() throws DBException{
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.DATABASEERROR.getMsg());
        Mockito.doThrow(Mockito.mock(DataAccessException.class)).when(jdbcTemplate).query(Mockito.anyString(),Mockito.any(BeanPropertyRowMapper.class));
        List<Stat> pStatListFakeReturned=statDaoSQLImple.getAllStatsByPlayer(0);

    }
}