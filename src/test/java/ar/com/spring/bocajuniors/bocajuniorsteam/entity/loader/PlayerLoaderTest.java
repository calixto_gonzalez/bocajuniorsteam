package ar.com.spring.bocajuniors.bocajuniorsteam.entity.loader;

import ar.com.spring.bocajuniors.bocajuniorsteam.dao.StatDaoSQLImple;
import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.management.j2ee.statistics.Stats;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class PlayerLoaderTest {
    private StatDaoSQLImple statDaoSQLImple;
    private PlayerLoader playerLoader;
    private List<Player> playerListFake;
    private List<Player> playerListFakeEmpty;


    @Before
    public void setup(){
        statDaoSQLImple=Mockito.mock(StatDaoSQLImple.class);
        playerLoader=new PlayerLoader(statDaoSQLImple);
        playerListFake= Arrays.asList(new Player(1, "sarasa", "lopez", "bolivia", 60));
        playerListFakeEmpty=new ArrayList<>();
    }

    @Test
    public void loadStatsShouldGetPlayers() throws DBException {
        doReturn(playerListFake).when(mock(PlayerLoader.class)).loadStats(Mockito.anyList());
        List<Player> playerListFakeReturned=playerLoader.loadStats(playerListFake);
        assertNotNull(playerListFakeReturned);
        assertEquals(playerListFake.size(),playerListFakeReturned.size());

    }

    @Test
    public void loadStatsShouldReturnEmptyArray() throws DBException {
        doReturn(new ArrayList<>()).when(mock(PlayerLoader.class)).loadStats(playerListFakeEmpty);
        List<Player> playerListFakeReturned=playerLoader.loadStats(playerListFakeEmpty);
        assertTrue(playerListFakeReturned.isEmpty());


    }

    @Test
    public void loadStatsShouldReturnNull() throws DBException {
        doReturn(null).when(mock(PlayerLoader.class)).loadStats(null);
        List<Player> playerListFakeReturned=playerLoader.loadStats(null);
        assertTrue(playerListFakeReturned.isEmpty());


    }

}