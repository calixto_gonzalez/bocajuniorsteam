package ar.com.spring.bocajuniors.bocajuniorsteam.entity;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class PlayerRequestTest {
    private PlayerRequest fakePlayerRequest;
    private PlayerRequest fakeEmptyPlayerRequest;
    private int fakeId;

    @Before
    public void setup() {
        fakeId=1;
        fakePlayerRequest=new PlayerRequest(fakeId);


    }

    @Test
    public void shouldSetIdandGet() {
        fakePlayerRequest.setIdPlayer(fakeId);
        assertEquals(fakeId,fakePlayerRequest.getIdPlayer());
    }
    @Test
    public void shouldBeEmpty(){
        fakeEmptyPlayerRequest=new PlayerRequest();
        assertNotNull(fakeEmptyPlayerRequest);

    }


}