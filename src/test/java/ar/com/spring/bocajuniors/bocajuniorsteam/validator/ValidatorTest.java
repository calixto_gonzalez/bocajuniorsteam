package ar.com.spring.bocajuniors.bocajuniorsteam.validator;

import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.StringException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Validate;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.util.Map;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doReturn;

public class ValidatorTest {
    @Rule
    public ExpectedException thrown=ExpectedException.none();


    private  Map countriesMapTest;
    private Countries countriesTest;
    private Validator validator;
    private String randomAlpha;
    private Validate validateTest;

    @Before
    public void setup () {
        countriesTest=Mockito.mock(Countries.class);
        validator=new Validator(countriesTest);
        randomAlpha=RandomStringUtils.randomAlphabetic(1,5);
        validateTest=Mockito.mock(Validate.class);




    }


    @Test
    public void validateStringShouldNotBeNullNorBlank() {
        doReturn(true).when(countriesTest).isValid(randomAlpha);
        validator.validateString(randomAlpha);
    }

    @Test
    public void validateStringShouldThrowIllegalArgumentExceptionIfBLANK() {
        thrown.expect(StringException.class);
        thrown.expectMessage(ErrorEnum.BLANK.getMsg());
        validator.validateString(" ");
    }
    @Test
    public void validateStringShouldThrowIllegalArgumentExceptionIfEMPTY() {
        thrown.expect(StringException.class);
        thrown.expectMessage(ErrorEnum.BLANK.getMsg());
        validator.validateString("");
    }

    @Test
    public void validateStringShouldThrowIllegalArgumentExceptionIfNULL() {
        thrown.expect(StringException.class);
        thrown.expectMessage(ErrorEnum.BLANK.getMsg());
        validator.validateString(null);

    }

    @Test
    public void validateStringShouldTrowStringExceptionIfNotAlpha() {
        thrown.expect(StringException.class);
        thrown.expectMessage(ErrorEnum.NOTLETTERS.getMsg());
        validator.validateString(Integer.toString(1337));
    }

    @Test
    public void validateStringShouldTrowStringExceptionIfCountryNotValid() {
        doReturn(false).when(countriesTest).isValid(anyString());
        thrown.expect(StringException.class);
        thrown.expectMessage(ErrorEnum.NOTACOUNTRY.getMsg());
        validator.validateString("Sarasa");
    }





}