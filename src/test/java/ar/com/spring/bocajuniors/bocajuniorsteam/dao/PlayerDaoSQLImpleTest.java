package ar.com.spring.bocajuniors.bocajuniorsteam.dao;

import ar.com.spring.bocajuniors.bocajuniorsteam.entity.Player;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.DBException;
import ar.com.spring.bocajuniors.bocajuniorsteam.exception.ErrorEnum;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class PlayerDaoSQLImpleTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private JdbcTemplate jdbcTemplate;
    private PlayerDaoSQL playerDaoSQL;
    private StatDaoSQLImple statDaoSQLImple;
    private List<Player> playerListFake;
    private Player playerMock;



    @Before
    public void setUp() throws Exception {
        jdbcTemplate = (Mockito.mock(JdbcTemplate.class));
        statDaoSQLImple = (Mockito.mock(StatDaoSQLImple.class));
        playerDaoSQL = (new PlayerDaoSQLImple(jdbcTemplate,statDaoSQLImple));
        playerListFake = (Arrays.
                asList(new Player(1, "Cristian", "Pavon", "Argentina", 22),
                        new Player(2, "Frank", "Fabra", "Argentina", 27)));
        playerMock = (new Player(1, "To Kill a Mockingbird", "Harper Lee", "United States", 89));

    }

    @Test
    public void getAllPlayersShouldGetPlayers() throws DBException, SQLException {


//        Mockito.doReturn(playerListFake.get(0)).when(getPlayerMapperMock()).mapRow(Mockito.any(ResultSet.class), Mockito.anyInt());
        Mockito.doReturn(playerListFake).when(jdbcTemplate).query(Mockito.anyString(), Mockito.any(BeanPropertyRowMapper.class));


        List<Player> playerList = playerDaoSQL.getAllPlayers();

        assertNotNull(playerList);
        assertEquals(playerList.size(), playerListFake.size());
        assertEquals(playerList.get(1).getName(), playerListFake.get(1).getName());


    }

    @Test
    public void getAllPlayersShouldThrowDBException() throws DBException {

        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.DATABASEERROR.getMsg());
        Mockito.doThrow(Mockito.mock(DataAccessException.class)).when(jdbcTemplate).query(Mockito.anyString(), Mockito.any(BeanPropertyRowMapper.class));
        playerDaoSQL.getAllPlayers();

    }

    @Test
    public void getPlayerByIdShouldGetPlayer() throws DBException {
        Mockito.doReturn(playerMock).when(jdbcTemplate).queryForObject(Mockito.anyString(),
                Mockito.any(Object[].class),
                Mockito.any(BeanPropertyRowMapper.class));
        Player player = playerDaoSQL.getPlayerById(1);
        assertEquals(playerMock.getName(), player.getName());
        assertNotNull(player);


    }

    @Test
    public void getPlayerByIdShouldThrowExceptionUSERNOTFOUNDERROR() throws DBException {
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        Mockito.doThrow(Mockito.mock(EmptyResultDataAccessException.class)).when(jdbcTemplate).queryForObject(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(BeanPropertyRowMapper.class));
        playerDaoSQL.getPlayerById(-1);


    }

    @Test
    public void getPlayerByIdShouldThrowExceptionDATABASEERROR() throws DBException {
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.DATABASEERROR.getMsg());
        Mockito.doThrow(Mockito.mock(DataAccessException.class)).when(jdbcTemplate).queryForObject(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(BeanPropertyRowMapper.class));
        playerDaoSQL.getPlayerById(-1);


    }

    @Test
    public void getPlayersByCountryShouldGetPlayerByCountry() throws DBException {
        Mockito.doReturn(playerListFake).when(jdbcTemplate).query(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(BeanPropertyRowMapper.class));


        List<Player> playerList = playerDaoSQL.getPlayersByCountry(playerListFake.get(0).getCountry());
        assertNotNull(playerList);
        assertEquals(playerList.size(), playerListFake.size());
        assertEquals(playerList.get(0).getName(), playerListFake.get(0).getName());
    }

    @Test
    public void getPlayerByCountryShouldThrowExceptionIfIdIsNull() throws DBException {

        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.USERNOTFOUNDERROR.getMsg());
        Mockito.doReturn(new ArrayList<>()).when(jdbcTemplate).query(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(BeanPropertyRowMapper.class));

        playerDaoSQL.getPlayersByCountry(null);


    }


    @Test
    public void getPlayerByCountryShouldThrowExceptionDataAccessException() throws DBException {
        thrown.expect(DBException.class);
        thrown.expectMessage(ErrorEnum.DATABASEERROR.getMsg());

        Mockito.doThrow(Mockito.mock(DataAccessException.class)).when(jdbcTemplate).query(Mockito.anyString(), Mockito.any(Object[].class), Mockito.any(BeanPropertyRowMapper.class));

        playerDaoSQL.getPlayersByCountry(null);


    }
    //TODO faltan las otras dos exceptions.

}